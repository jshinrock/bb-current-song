module.exports = function(grunt) {
    grunt.initConfig ({
        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['js/**/*.js'],
                // the location of the resulting JS file
                dest: 'public/js/app.min.js'
            }
        },
        sass: {
            dist: {
                files: {
                'public/stylesheets/style.css' : 'sass/style.scss',
                'public/stylesheets/normalize.css': 'node_modules/foundation/scss/normalize.scss',
                'public/stylesheets/foundation.css': 'node_modules/foundation/scss/foundation.scss',
                }
            }
        },
        watch: {
            source: {
                files: ['sass/**/*.scss','node_modules/foundation/scss/*.scss'],
                tasks: ['sass']
            }
        }
    });
    

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default', ['sass','concat']);
};